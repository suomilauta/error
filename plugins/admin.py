import sys

def setup(bot, config):
	bot.hook('\!die', bot_die, ('die', 'kills the bot'))
	bot.hook('\!reload', bot_reload, ('reload', 'reloads all plugins'))
	bot.hook('\!hooks', bot_hooks, ('hooks', 'show all command hooks'))

def bot_die(bot, user, channel, msg, matches):
	if user in bot.config.trusted:
		bot.quit("mo :D")
		sys.exit(1)

def bot_reload(bot, user, channel, msg, matches):
	if user in bot.config.trusted:
		bot.load_plugins()
		bot.reply(user, channel, "done")

def bot_hooks(bot, user, channel, msg, matches):
	if user in bot.config.trusted:
		bot.reply(user, channel, str(bot.hooks))