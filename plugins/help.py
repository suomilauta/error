def setup(bot, config):
	bot.hook('\!?\.?help ?(.+)?', help, ('help [command]', 'list commands or get help on a command'))
	print("Help setup triggered")

def help(bot, user, channel, msg, matches):
	query = matches.group(1)
	if query:
		results = []
		for _, _, (command, helptext) in bot.hooks:
			if query in command or query in helptext:
				results.append((command, helptext))
	else:
		results = [h for _, _, h in bot.hooks]
	print(results)

	if not results:
		bot.reply(user, channel, 'command does not exist')
		return

	results = sorted(results)
	longest_command = max(len(command) for command, _ in results)

	reply = "\n".join("%s - %s" % (command.ljust(longest_command), helptext)
					  for command, helptext in results)

	bot.reply(user, channel, reply)
