#!/usr/bin/env python
# -*- coding: utf-8 -*-

import importlib, sys, codecs, time, random, re

from twisted.internet import reactor, protocol, defer
from twisted.words.protocols import irc
from twisted.python import log

server = ['irc.quakenet.org',6667]

channels = ['#lapio'] 
plugins = {'help': [], 'admin': []}
plugindir = 'plugins.'

config = []
ircnames = []

class Bot(irc.IRCClient):
	def __init__(self, config, *args, **kwargs):
		self._namescallback = {}
		self.config = config

		self.nickname = self.config.nick
		self.channels = self.config.channels
		self.realname = self.config.realname

		self._init_plugins()

	def hook(self, regex, callback, help=None):
		self.hooks.append((regex, callback, help))

	def _init_plugins(self):
		self.hooks = []
#		print(self.config.plugins.items())
		for pname, pconf in self.config.plugins.items():
			pname = plugindir+pname
			print("Loading plugin %s" % pname)
			try:
				plugin = importlib.import_module(pname)
			except Exception as e:
				print("Error loading plugin %s: %s" % (pname, str(e)))
				raise e
			pconfig = []
			pconfig.append(self.config)
#			print(plugin)
			pconfig.append(pconf)
			plugin.setup(self, pconfig)

	def got_names(self,nicklist):
		self.ircnames = nicklist
		self.ircnames = [s.replace('@','') for s in nicklist]
		self.ircnames = [s.strip('+') for s in nicklist]
		print(self.ircnames)

	def names(self, channel):
		channel = channel.lower()
		d = defer.Deferred()
		if channel not in self._namescallback:
			self._namescallback[channel] = ([], [])

		self._namescallback[channel][0].append(d)
		self.sendLine("NAMES %s" % channel)
		return d

	def irc_RPL_NAMREPLY(self, prefix, params):
		channel = params[2].lower()
		nicklist = params[3].split(' ')

		if channel not in self._namescallback:
			return

		n = self._namescallback[channel][1]
		n += nicklist

	def irc_RPL_ENDOFNAMES(self, prefix, params):
		channel = params[1].lower()
		if channel not in self._namescallback:
			return

		callbacks, namelist = self._namescallback[channel]

		for cb in callbacks:
			cb.callback(namelist)

		del self._namescallback[channel]
        
	def msg(self, user, message, length=None):
		message = message.rstrip()

		if message:
			return irc.IRCClient.msg(self,user,message,length)

	def say(self, channel, message):
		message = message.rstrip()

		if message:
			return irc.IRCClient.say(self,channel,message)

	def reply(self, user, channel, message):
		message = message.rstrip()
		who = user if channel == self.nickname else channel
		print(">>> [%s] %s" % (who, message))
		self.msg(who, message)

	def connectionMade(self):
		irc.IRCClient.connectionMade(self)
		print("Connected")
		
	def connectionLost(self,reason):
		print("Disconnected: %s" % reason)
		irc.IRCClient.connectionLost(self,reason)
		
	def signedOn(self):
		print("Signed on as %s" % self.nickname)
		for channel in self.config.channels:
			self.join(channel)
			
	def userJoined(self,user,channel):
		nick = user.split('!')[0]
		self.ircnames.append(nick)
			
	def joined(self, channel):
		print("Joined %s" % channel)
		self.names(channel).addCallback(self.got_names)
		if not channel in self.channels:
			self.channels.append(channel)
		print(self.names(channel))
		
	def left(self,channel):
		self.channels.remove(channel)

	def privmsg(self,user,channel,msg):
		#msg = msg.decode('utf-8')
		who = user if channel == self.nickname else channel
		nick = user.split('!')[0]

		print("<<< [%s] <%s> %s" % (who, nick,msg))

		if nick == self.nickname:
			return

		# Plugin hooks

		for (regex, callback, helptext) in self.hooks:
			try:
				matches = re.search(regex,msg)
				if matches:
					callback(self, user, channel, msg, matches)
			except Exception as e:
				if e == SystemExit:
					sys.exit(1)
				self.reply(user, channel, 'oho: %s' % str(e))
			

class BotFactory(protocol.ClientFactory):
	def __init__(self, bot_class, config):
		self.config = config
		self.bot_class = bot_class
		self.protocol = protocol = bot_class(config)

	def buildProtocol(self, addr):
		self.bot = self.bot_class(self.config)
		self.bot.factory = self
		return self.bot

	def clientConnectionLost(self, connector, reason):
		print("Connection lost. Reason: %s" % reason)
		sys.exit(1)

	def clientConnectionFailed(self, connector, reason):
		print("Connection failed. Reason: %s" % reason)
		
def main():
	from optparse import OptionParser
	parser = OptionParser(usage="usage: %prog [options] channels")

	parser.add_option("-s", "--server", action="store", dest="server", default="irc.quakenet.org", help="IRC server to connect to")
	parser.add_option("-p", "--port", action="store", type="int", dest="port", default=6667, help="IRC server to connect to")
	parser.add_option("-n", "--nick", action="store", dest="nick", default="error", help="Nickname to use")
	parser.add_option("-r", "--realname", action="store", dest="realname", default="error", help="Realname to use")
	parser.add_option("--password", action="store", dest="password", default=None, help="server password")
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="enable verbose output")
	parser.add_option("-t", "--trust", action="append", dest="trusted", default=["skittie!emilia@nupit.se8.fi", "skittie!emilia@se8.fi", "skittie!emilia@178.213.233.234"], help="trusted hostmasks (admin)")

	(config, config.channels) = parser.parse_args()

	if not config.channels:
		config.channels = channels

	config.plugins = plugins

	if config.verbose:
		log.startLogging(sys.stdout)
	
	factory = BotFactory(Bot, config)
	factory.protocol = Bot
	reactor.connectTCP(config.server, config.port, factory)
	reactor.run()

if __name__ == "__main__":
	main()
